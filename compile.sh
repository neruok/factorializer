#!/bin/sh

# get flags for cmake
flags=`cat ./flags.txt | grep -v "#" | grep "\-D"`
flags=`echo $flags`

# clean the build directory
mkdir -p build
rm -r build
mkdir -p build
cd build

# build
cmake $flags ..
make -j `nproc`
