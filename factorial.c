/*
 * Factorializer
 *
 * Copyright (C) 2018 Cody Wentz
 *
 * Factorializer is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Factorializer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Cody Wentz <codywentz@gmail.com>
 */

#include <gmp.h>
#include <stdlib.h>
#include <stdio.h>

void mpz_fac(mpz_t result, mpz_t input ){
    // the temp variable will be decremented.
    mpz_t temp;
    mpz_init_set(temp, input);
    mpz_set_ui(result, 1);

    // same decrement as in main.
    while(mpz_cmp_ui(temp, 0) > 0 ) {
        mpz_mul(result, result, temp);
        mpz_sub_ui(temp, temp, 1);
        // redirect stderr to /dev/null to speed things along.
        fprintf (stderr, "\tstep: ");
        mpz_out_str(stderr,10,result);
        fprintf (stderr, "\n");
    }
}

int main(size_t argc, char ** argv) {
    char * input_num;
    char * factorial_operations;

    // handle args
    size_t max_iter;
    switch (argc) {
        case 3: {
            // argv[0] is the name of program
            input_num = argv[1];
            factorial_operations = argv[2];
            break;
        }
        default: {
            // not yet implemented
            return 1;
        }
    }

    mpz_t input;
    mpz_t factorial_count;

    // initialize with
    mpz_init_set_str(input, input_num, 10);
    mpz_init_set_str(factorial_count, factorial_operations, 10);

    // countdown the factorial operations until 0.
    // mpz_cmp_ui(op1,op2)
    // (op1 >  op2) == +1
    // (op1 <  op2) == -1
    // (op1 == op2) == 00
    while(mpz_cmp_ui(factorial_count, 0) > 0 ){
        // redirect stderr to /dev/null to speed things along.
        fprintf(stderr, "fac: ");
        mpz_out_str(stderr,10,input);
        fprintf (stderr, "\n");

        // (result, left, right)
        mpz_sub_ui(factorial_count, factorial_count, 1);

        // result, input
        mpz_fac(input, input);
    }

    // print result.
    mpz_out_str(stdout,10,input);
    printf ("\n");

}
